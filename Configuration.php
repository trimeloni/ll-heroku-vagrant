<?php

namespace CONF;

/**
 * Description of CONF
 *
 * @author Atrimeloni
 *
 * Used to store Configuration Data
 */
class Configuration {
 # Mail Setup
 const MAIL_SEND    = FALSE;
 const MAIL_HOST    = 'smtp.gmail.com';
 const MAIL_SMTP_AUTH = true;
 const MAIL_USER    = '@gmail.com';
 const MAIL_PASSWORD = '';
 const MAIL_SMTP_SECURE_METHOD = 'tls';
 const MAIL_SMTP_PORT = 587;
 const MAIL_FROM_ADDRESS  = 'no-reply@...';
 const MAIL_FROM_NAME = 'MAILER';
 const MAIL_TO_ADDRESS = 'testing@...';
 const MAIL_REPLY_TO_ADDRESS = 'no-reply@...';
 const MAIL_REPLY_TO_NAME = '... No-Reply';
 const MAIL_SUBJET = '...';


 # DataBase Setup
 const DB_HOST      = 'localhost';
 const DB_SCHEMA    = 'video';
 const DB_USERNAME = 'vagrant';
 const DB_PASSWORD = 'vagrant';
 const DB_PORT = '3306';

}

?>
