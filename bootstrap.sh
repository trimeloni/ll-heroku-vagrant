#! /usr/bin/env bash

###
#
# bootstrap.sh
#
# This script assumes your Vagrantfile has been configured to run the bootstrap.sh file
#
###
GITREPO="https://$GITUSER:$GITPASS@bitbucket.org/trimeloni/ll-heroku-code.git"


# Setup Options
#true/false
BASEPACKAGES="true"
LINKTOREPOS="true"


# Local Variables
DBUSER=vagrant
DBPASSWD=vagrant
DBSCHEMA=video
IPADDRESS=33.33.33.110


if [ "$BASEPACKAGES" == "true" ]; then

  echo -e "\n--- Okay, installing Packages now... ---\n"

  echo -e "\n--- Updating packages list ---\n"
  apt-get -qq update

  echo -e "\n--- Install base packages ---\n"
  apt-get -y install vim curl build-essential python-software-properties git >> /vagrant/vm_build.log 2>&1

  #
  # MySQL
  #

  # MySQL setup for development purposes ONLY
  echo -e "\n--- Install MySQL specific packages and settings ---\n"
  debconf-set-selections <<< "mysql-server mysql-server/root_password password $DBPASSWD"
  debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $DBPASSWD"
  
  # MyPhpAdmin setup for development purposes ONLY
  debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
  debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password $DBPASSWD"
  debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $DBPASSWD"
  debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password $DBPASSWD"
  debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none"
  
  # Install MySQL
  apt-get -y install mysql-server phpmyadmin >> /vagrant/vm_build.log 2>&1
  
  echo -e "\n--- Setting up our MySQL user and db ---\n"
  sed -i "s/bind-address/#bind-address/g" /etc/mysql/my.cnf  >> /vagrant/vm_build.log 2>&1
  mysql -uroot -p$DBPASSWD -e "CREATE DATABASE $DBNAME" >> /vagrant/vm_build.log 2>&1
  mysql -uroot -p$DBPASSWD -e "grant all privileges on *.* to '$DBUSER'@'%' identified by '$DBPASSWD' WITH GRANT OPTION;" >> /vagrant/vm_build.log 2>&1

  #Install PostGreSQL
  apt-get -y install postgresql


  #
  # Php/Apache
  #
  echo -e "\n--- Installing PHP-specific packages ---\n"
  apt-get -y install php5 apache2 libapache2-mod-php5 php5-curl php5-gd php5-mcrypt >> /vagrant/vm_build.log 2>&1

  echo -e "\n--- Enabling mod-rewrite ---\n"
  a2enmod rewrite >> /vagrant/vm_build.log 2>&1

  echo -e "\n--- Allowing Apache override to all ---\n"
  sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf

  echo -e "\n--- Setting document root to public directory ---\n"
  #rm -rf /var/www/html
  #ln -fs /vagrant/public /var/www/html

  echo -e "\n--- We definitly need to see the PHP errors, turning them on ---\n"
  sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php5/apache2/php.ini
  sed -i "s/display_errors = .*/display_errors = On/" /etc/php5/apache2/php.ini

  echo -e "\n--- Restarting Apache ---\n"
  service apache2 restart >> /vagrant/vm_build.log 2>&1

  echo -e "\n--- Installing Composer for PHP package management ---\n"
  curl --silent https://getcomposer.org/installer | php >> /vagrant/vm_build.log 2>&1
  mv composer.phar /usr/local/bin/composer

  #
  # Apache - PHP/MySQL Packages
  #
  apt-get -y libapache2-mod-auth-mysql php5-mysql >> /vagrant/vm_build.log 2>&1
  



  #
  # Adding HappyCow MySQL
  #

  # Create Schema, Create Table
  mysqladmin -u$DBUSER -p$DBPASSWD create $DBSCHEMA
  mysql -u$DBUSER -p$DBPASSWD $DBSCHEMA< /vagrant/createtables.sql

fi


#
# Link Webcache, www, and Config Folders to Git for easier development/checkin
#
if [ "$LINKTOREPOS" == "true" ]; then
  echo -e "\n--- Link Apache to Git Repos ---\n"

  # git www
  if [ ! -e /var/www.old ]; then
    
    sudo -H -u vagrant mkdir -p /home/vagrant/git/3z
    sudo -H -u vagrant git clone $GITREPO /home/vagrant/git/3z/vid  >> /vagrant/vm_build.log 2>&1
    chown -R vagrant:vagrant /home/vagrant/git
    
    chown -R vagrant:vagrant /var/www
    mv /var/www/html /var/www/html.old
    ln -s /home/vagrant/git/3z/vid/www/ /var/www/html
    ln -s /home/vagrant/git/3z/vid/Project/ /var/www/Project
  fi
  
  if [ -e /vagrant/Configuration.php ]; then
    cp /vagrant/Configuration.php  /home/vagrant/git/3z/vid/Project/CONF/Configuration.php  >> /vagrant/vm_build.log 2>&1
    chown -R vagrant:vagrant /home/vagrant/git/3z/vid/Project/CONF  >> /vagrant/vm_build.log 2>&1
  fi
  
  # run composer - to download the dependencies
  cd /home/vagrant/git/3z/vid/Project  >> /vagrant/vm_build.log 2>&1
  sudo -H -u vagrant composer install  >> /vagrant/vm_build.log 2>&1

fi


echo -e "\n"
echo -e "---------------------------------------------------------" 
echo -e "--- Everything is done, here are some things to know: ---"
echo -e "--- SSH into $IPADDRESS for development. ---"
echo -e "--- User/Password: vagrant/vagrant ---"
echo -e "--- MySQL root user/password: $DBUSER/$DBPASSWD ---"
echo -e "--- Goto: http://$IPADDRESS in your browser! ---"
echo -e "--- Goto: http://33.33.33.110/phpmyadmin/ in your browser! ---"
echo -e "---------------------------------------------------------" 

