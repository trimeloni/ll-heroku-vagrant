CREATE TABLE `contact` (
 `contactId` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(255) NOT NULL,
 `email` varchar(255),
 `phone` varchar(255),
 `eventDate` varchar(255),
 `eventType` varchar(255),
 `eventLength` varchar(255),
 `comment` varchar(255),

 `createdDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`contactId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
