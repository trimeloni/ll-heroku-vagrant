hnvB9Sz6BqZDqaWEjbPf

Pre-Requisites
  Local
    git
    vagrant
    virtual box
  
  Internet
    Heroku Account (www.heroku.com)
    


-- Local Setup First - Getting some Sample PHP Code --
Create diretory to hold the files in
  mkfir C:\dev\ll-heroku
  cd C:\dev\ll-heroku
  
Clone the Vagrant Files
  git clone git@bitbucket.org:trimeloni/ll-heroku-vagrant.git

Start the Virtual Machone
  vagrant up
  


-- Heroku --
L&L - Heroku - Php - Composer - Git
https://devcenter.heroku.com/articles/getting-started-with-php#introduction

-- Pre-pare existing application for Heroku --
Remove the Old Demo App

# composer updates
composer files
  composer* files
  composer.json
    Project fir
  composer update

# vendor autoloader 
  # has two references - one to autolader and one to teh BLL dir
  # since the configuration auto-scans that dir (istead of using a config file)
  www/api/dispatch.php


# heroku procfile updates
edit the ProcFile to use www/

# git commits
git commit 

# heroku creation
heroku create
git push heroku master

# scale the dyno
heroku ps:scale web=1


